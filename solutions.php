<!DOCTYPE html>
<html lang="en">
<head>
<?php include 'include/header.php'?>

<title>解决方案 - 云擎，移动网站专家</title>
</head>
<body>
	<?php include 'include/navigation_bar.php';?>

	<?php include 'include/carousel.php';?>	

	<!-- Marketing messaging and featurettes
    ================================================== -->
	<!-- Wrap the rest of the page in another container to center all the content. -->
	<div class="container marketing">
		<!-- Three columns of text below the carousel -->
		<!-- /.row -->


		<!-- START THE FEATURETTES -->
		<div class="row featurette">
			<div class="col-md-5">
				<a id="site" name="site"></a>
				<img id="siteImage" class="featurette-image img-responsive"
					data-src="holder.js/500x500/auto" alt="500x500">
			</div>
			<div class="col-md-7">
				<h2 class="featurette-heading">云擎DME™双模网站</h2>
				<p class="lead">基于云计算平台，采用最先进的双模引擎DME™（Dual Mode
					Engine），结合最前沿的HTML5技术，让你的新网站一次性适配PC与全部移动终端，同时更有利于搜索引擎抓取，让企业品牌在PC互联与移动互联时代，无缝对接，一网打尽。</p>
				<p class="lead">云擎DME™双模网站 化繁为简，去粗存精，专注极致用户体验。让你的公司与品牌形象直接占领用户心智，使你在行业里与众不同，比竞争对手更胜一筹。</p>
			</div>
		</div>
		<hr class="featurette-divider">
		<div class="row featurette">
			<div class="col-md-7">
				<a id="mobile" name="mobile"></a>
				<h2 class="featurette-heading">云智配CSP™移动化</h2>
				<p class="lead">基于微软AZURE云计算平台，采用自主研发的CSP™代码与ALMB架构，将具有深度交互、数据库、支付等高级复杂功能的网站转化为移动网站，适配全部移动终端。</p>
				<p class="lead">云智配™CSP™移动化方案具有最优化的人机交互体验+UI界面，增强用户体验愉悦性，提高用户粘性，比WAP建站、APP客户端更快、更好、更安全，是关注企业与品牌形象、重视用户体验的企业与机构的明智选择。</p>
			</div>
			<div class="col-md-5">
				<img id="mobilizeImage" class="featurette-image img-responsive"
					data-src="holder.js/500x500/auto" alt="500x500">
			</div>
		</div>
		<hr class="featurette-divider">
		<div class="row featurette">
			<div class="col-md-5">
				<a id="marketing" name="marketing"></a>
				<img id="marketImage" class="featurette-image img-responsive"
					data-src="holder.js/500x500/auto" alt="500x500">
			</div>
			<div class="col-md-7">
				<h2 class="featurette-heading">云推广•整合营销系统</h2>
				<p class="lead">一套能在网上跑业务的系统，整合网络营销推广第1品牌。</p>
				<ul class="list-group">
					<li><h4>【云网站-跑业务】批量一次性建设100个营销型网站，12小时实现百度等搜索引擎排名，利用云搜索无限制设置关键词！</h4></li>
					<li><h4>【云广告-接订单】批量一次性在上百家分类信息网站与B2B商贸网站上进行信息的发布！大幅度提升企业的信息曝光度！</h4></li>
					<li><h4>【云  盟-抢订单】一分钟在你竞争对手在网上发布的信息广告旁批量进行广告展示，把广告打到你的竞争对手的广告上！</h4></li>
					<li><h4>【云新闻-建品牌】批量一次性在新浪等500家网络媒体上随机进行软文报道！提升企业的全网影响力，为企业强力打造网络品牌！</h4></li>
				</ul>
			</div>
		</div>
		<hr class="featurette-divider">

		<!-- /END THE FEATURETTES -->

		<!-- START THE ABOUNT US -->

		<!-- <h2>关于我们</h2> -->

	</div>
	<!-- /.container -->
	
	<?php include 'include/footer.php';?>
</body>
	
	<script type="text/javascript">
		$(document).ready(function(){

			$("#solutionNav").addClass("active");
			
			//Display images of solution
			$("#siteImage").attr('src', "images/solution-site.jpg");
			$("#siteImage").attr('data-src', "");
			$("#mobilizeImage").attr('src', "images/solution-mobilize.jpg");
			$("#mobilizeImage").attr('data-src', "");
			$("#marketImage").attr('src', "images/solution-market.jpg");
			$("#marketImage").attr('data-src', "");
			
			commonUpdate();
			
		});
	</script>
</html>
