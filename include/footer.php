	<!-- FOOTER -->
	<footer class="footer links" id="navFooterDiv">
		<div class="container">
			<div class="row">
				<div class="col-lg-3">
					<ul>
					<h4>解决方案</h4>
						<li><a href="solutions.php#site">云擎DME™双模网站</a></li>
						<li class="hide"><a href="#">如何运作</a></li>
						<li><a href="solutions.php#mobile">云智配CSP™移动化</a></li>
						<li><a href="solutions.php#marketing">云推广•整合营销系统</a></li>
					</ul>
				</div>
				<div class="col-lg-3">
					<ul>
					<h4>关于我们</h4>
						<li><a href="/aboutus.php#start">关于我们</a></li>
						<li class="hide"><a href="partners.php">合作伙伴</a></li>
						<li class="hide"><a href="http://blog.allmobilize.com"
							target="_blank">我们的博客</a></li>
						<li><a href="/aboutus.php#joinus">加入我们</a></li>
					</ul>
				</div>
				<div class="col-lg-3">
					<ul>
					<h4>联系我们</h4>
						<li><a href="mailto:support@yungine.com">技术支持</a></li>
						<li><a href="aboutus.php#contactus">联系我们</a></li>
					</ul>
				</div>
				<div class="col-lg-3">
					<ul>
					<h4>客户案例</h4>
						<li><a href="http://www.moschina.com.cn" target="_blank">全球餐饮品牌连锁-摩斯汉堡</a></li>
						<li><a href="http://www.fmhgz.cn" target="_blank">香港泛美泓涵国际美容集团</a></li>
						<li><a href="http://damufang.com.cn" target="_blank">CCTV合作伙伴-大木坊</a></li>
						<!-- <li><a href="cases.php#start">更多案例...</a></li> -->
					</ul>
				</div>
			</div>
		</div>
		<!-- <p class="pull-right"><a href="#">Back to top</a></p> -->
	</footer>
	
	<footer class="footer newsletter">
		<div class="container" id="copyrightPCDiv">
			<div class="text-center">
				&copy; 2014 云擎, 德厚信息科技成员 &middot; 版权所有 粤ICP备09006781号
				<!-- <a href="#">Privacy</a> &middot; <a href="#">Terms</a> -->
			</div>
		</div>
		<div class="container" id="copyrightMobileDiv">
			<div class="text-center">
				&copy; 2014 云擎, 德厚信息科技成员 &middot; 版权所有
				<!-- <a href="#">Privacy</a> &middot; <a href="#">Terms</a> -->
			</div>
			<div class="text-center">粤ICP备O9OO6781号</div>
		</div>
	</footer>

	<!-- Bootstrap core JavaScript
    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script src="js/jquery-1.10.2.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/holder.js"></script>
	<script src="js/my.js"></script>