	<div class="navbar-wrapper">
		<div class="container">

			<div id="navbarDiv" class="navbar navbar-inverse navbar-fixed-top" role="navigation">
				<div class="container">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse"
							data-target=".navbar-collapse">
							<span class="sr-only">Toggle navigation</span> <span
								class="icon-bar"></span> <span class="icon-bar"></span> <span
								class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="http://www.yungine.com">云擎YunGine</a>
					</div>
					<div class="navbar-collapse collapse">
						<ul class="nav navbar-nav">
							<li id="homeNav" class="navfont"><a href="index.php">首页</a></li>
							<li id="solutionNav" class="dropdown navfont"><a href="#" class="dropdown-toggle"
								data-toggle="dropdown">解决方案<b class="caret"></b></a>
								<ul class="dropdown-menu">
									<li class="navfont"><a href="solutions.php#site">云擎DME™双模网站</a></li>
									<li class="navfont"><a href="solutions.php#mobile">云智配CSP™移动化</a></li>
									<li class="navfont"><a href="solutions.php#marketing">云推广·整合营销系统</a></li>
								</ul></li>
							<li id="caseNav" class="navfont"><a href="cases.php#start">客户案例</a></li>
							<li id="channelNav" class="navfont"><a href="channel.php#start">渠道合作</a></li>
							<li id="aboutNav" class="navfont"><a href="aboutus.php#start">关于我们</a></li>
						</ul>
					</div>
				</div>
			</div>

		</div>
	</div>