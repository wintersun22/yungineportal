

	<!-- Carousel
	================================================== -->
	<div id="myCarousel" class="carousel slide" data-ride="carousel">
		<!-- Indicators -->
		<ol class="carousel-indicators">
			<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
			<li data-target="#myCarousel" data-slide-to="1"></li>
			<li data-target="#myCarousel" data-slide-to="2"></li>
		</ol>
		<div class="carousel-inner">
			<div class="item active">
				<img id="banner1Image"
					data-src="holder.js/900x500/auto/#777:#7a7a7a/text:品牌建站" alt="品牌建站">
				<div class="container">
					<div class="carousel-caption">
						<h3>云擎DME™双模网站</h3>
						<p>PC与移动无缝对接，移网打尽！</p>
						<!-- <p>
								<a class="btn btn-lg btn-primary" href="#" role="button">马上登记</a>
								</p> -->
					</div>
				</div>
			</div>
			<div class="item">
				<img id="banner2Image"
					data-src="holder.js/900x500/auto/#666:#6a6a6a/text:移动建站"
					alt="云智配TSP™专业移动建站">
				<div class="container">
					<div class="carousel-caption">
						<h3>云智配专业移动建站</h3>
						<p>将PC网站快速转化为移动网站</p>
						<p>把握移动互联，赢得下个10年！</p>
						<!-- <p>
								<a class="btn btn-lg btn-primary" href="#" role="button">即刻开始</a>
								</p> -->
					</div>
				</div>
			</div>
			<div class="item">
				<img id="banner3Image"
					data-src="holder.js/900x500/auto/#555:#5a5a5a/text:G3云推广"
					alt="G3云推广">
				<div class="container">
					<div class="carousel-caption">
						<h3>整合营销系统</h3>
						<p>整合网络营销推广第1品牌</p>
						<p>高精准、高覆盖、高转化</p>
					</div>
				</div>
			</div>
		</div>
		<a class="left carousel-control" href="#myCarousel" data-slide="prev"><span
			class="glyphicon glyphicon-chevron-left"></span></a> <a
			class="right carousel-control" href="#myCarousel" data-slide="next"><span
			class="glyphicon glyphicon-chevron-right"></span></a>
	</div>
	<!-- /.carousel -->