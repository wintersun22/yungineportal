<!DOCTYPE html>
<html lang="en">
<head>
<?php include 'include/header.php'?>
<title>客户案例 - 云擎，移动网站专家</title>
</head>
<body>
	<?php include 'include/navigation_bar.php';?>

	<?php include 'include/carousel.php';?>	
	
	<div class="container marketing">
		<div class="panel-body">
			<a id="start"></a>
			<h4 class="askstyle">为什么这么多年来，我们源源不断有老客介绍过来的新客户？</h4>
			<h4 class="askstyle">为什么我们经常能接到客户的第2个甚至是第3个建站合同？</h4>
			<h4 class="askstyle">为什么华南、甚至全国有N多公司把他们的项目转交给我们？</h4>
			<h3>
对此，我们不想解释太多，我们可以告诉你的是：
不是每一个网络公司，都有超过1000个优质客户案例！</h3>
		</div>
		<div class="row">
			<div class="col-lg-4">
	          	<a href="http://www.iwillhave.net" target="_blank"><img class="img-rounded" src="images/case-iwillhave.png" alt="苹果高端配件iwill" 
	          		style="width: 300px; height: 215px;"></a>
	          	<h4><a href="http://www.iwillhave.net" target="_blank">苹果高端配件iwill</a></h4>
        	</div>
			<div class="col-lg-4">
	          	<a href="http://bestquality.com.cn/" target="_blank"><img class="img-rounded" src="images/case-best-quality.png" alt="CCTV上榜品牌-NO LIMIT" 
	          		style="width: 300px; height: 215px;"></a>
	          	<h4><a href="http://bestquality.com.cn/" target="_blank">CCTV上榜品牌-Best Quality</a></h4>
        	</div>
			<div class="col-lg-4">
	          	<a href="http://damufang.com.cn" target="_blank"><img class="img-rounded" src="images/case-damufang.png" alt="CCTV合作伙伴-大木坊" 
	          		style="width: 300px; height: 215px;"></a>
	          	<h4><a href="http://damufang.com.cn" target="_blank">CCTV合作伙伴-大木坊</a></h4>
        	</div>
		</div>
		<div class="row">
			<div class="col-lg-4">
	          	<a href="http://www.moschina.com.cn" target="_blank"><img class="img-rounded" src="images/case-moschina.png" alt="特色餐饮品牌连锁-摩斯汉堡" 
	          		style="width: 300px; height: 215px;"></a>
	          	<h4><a href="http://www.moschina.com.cn" target="_blank">特色餐饮品牌连锁-摩斯汉堡</a></h4>
        	</div>
			<div class="col-lg-4">
	          	<a href="http://www.giorman.com" target="_blank"><img class="img-rounded" src="images/case-giorman.png" alt="小家电出口大户Giorman" 
	          		style="width: 300px; height: 215px;"></a>
	          	<h4><a href="http://www.giorman.com" target="_blank">小家电出口大户Giorman</a></h4>
        	</div>
			<div class="col-lg-4">
	          	<a href="http://www.xianhuayuding.com/" target="_blank"><img class="img-rounded" src="images/case-xianhuayuding.png" alt="B2C垂直电商-鲜花预定网" 
	          		style="width: 300px; height: 215px;"></a>
	          	<h4><a href="http://www.xianhuayuding.com/" target="_blank">B2C垂直电商-鲜花预定网</a></h4>
        	</div>
        </div>
		<div class="row">
			<div class="col-lg-4">
	          	<a href="http://www.gmgs.com.cn/" target="_blank"><img class="img-rounded" src="images/case-gmgs.png" alt="国土资源部广州某局" 
	          		style="width: 300px; height: 215px;"></a>
	          	<h4><a href="http://www.gmgs.com.cn/" target="_blank">国土资源部广州某局</a></h4>
        	</div>
			<div class="col-lg-4">
	          	<a href="http://www.gdfii.com/" target="_blank"><img class="img-rounded" src="images/case-gdfii.png" alt="广东食品工业研究所" 
	          		style="width: 300px; height: 215px;"></a>
	          	<h4><a href="http://www.gdfii.com/" target="_blank">广东食品工业研究所</a></h4>
        	</div>
			<div class="col-lg-4">
	          	<a href="http://www.liby.com.cn/" target="_blank"><img class="img-rounded" src="images/case-liby.png" alt="知名企业-立白集团" 
	          		style="width: 300px; height: 215px;"></a>
	          	<h4><a href="http://www.liby.com.cn/" target="_blank">知名企业-立白集团</a></h4>
        	</div>
        </div>
        <h3 class="text-center"><a href="http://teqhost.cn/case/" target="_blank" id="moreCasesDiv">更多客户案例......</a></h3>
	</div>

	<?php include 'include/footer.php';?>
</body>
	
	<script type="text/javascript">
		$(document).ready(function() {
			commonUpdate();

			$("#caseNav").addClass("active");
			
			if(isMobile){
				//$("#moreCasesDiv").href = "http://m.teqhost.cn/qiyeanli";
				$("#moreCasesDiv").attr('href', "http://m.teqhost.cn/qiyeanli");
			}
		});
	</script>
</html>
