<!DOCTYPE html>
<html lang="en">
<head>
<?php require 'include/header.php'?>
<title>云擎, 广州建站, 移动建站, 云推广</title>
</head>
<body>
	<?php include 'include/navigation_bar.php';?>

	<?php include 'include/carousel.php';?>

	<!-- Marketing messaging and featurettes
    ================================================== -->
	<!-- Wrap the rest of the page in another container to center all the content. -->
	<div class="container marketing" id="featuresDiv">

		<!-- START THE FEATURETTES -->

		<div class="row">
			<div class="col-lg-4">
				<img class="img-rounded" alt="基于云计算平台服务"
					src="images/features-1.png" style="width: 210px; height: 150px;"/>
				<h4>基于云计算平台服务</h4>
				<h4>更安全、更快速、更高效</h4>
			</div>
			<!-- /.col-lg-4 -->
			<div class="col-lg-4">
				<img class="img-rounded" alt="更有利于搜索引擎抓取"
					src="images/features-2.png" style="width: 134px; height: 150px;"/>
				<h4>更有利于搜索引擎抓取</h4>
				<h4>给企业带来更多市场机会</h4>
			</div>
			<!-- /.col-lg-4 -->
			<div class="col-lg-4">
				<img class="img-rounded" alt="适配所有安卓、苹果移动终端"
					src="images/features-3.png" style="width: 201px; height: 150px;"/>
				<h4>适配所有安卓、苹果移动终端</h4>
				<h4>把握移动互联趋势，赢得下个10年</h4>
			</div>
			<!-- /.col-lg-4 -->
		</div>

		<!-- /END THE FEATURETTES -->

	</div>
	<!-- /.container -->

	<div class="btn-list" id="navButtonsDiv">
		<div class="ui-grid-a">
			<div class="ui-block-a">
				<a href="solutions.php#site" data-role="button"
					data-theme="a" data-corners="false" data-shadow="true"
					data-iconshadow="true" data-wrapperels="span"
					class="ui-btn ui-shadow ui-btn-up-a"><span class="ui-btn-inner"><span
						class="ui-btn-text"> <img src="/images/icon-features.png"><span>解决方案</span></span></span></a>
			</div>
			<div class="ui-block-b">
				<a href="cases.php#start" data-role="button"
					data-theme="a" data-corners="false" data-shadow="true"
					data-iconshadow="true" data-wrapperels="span"
					class="ui-btn ui-shadow ui-btn-up-a"><span class="ui-btn-inner"><span
						class="ui-btn-text"> <img src="/images/icon-why.png"><span>客户案例</span></span></span></a>
			</div>
		</div>
		<div class="ui-grid-a">
			<div class="ui-block-a">
				<a href="aboutus.php#contactus" data-role="button" data-theme="a"
					data-corners="false" data-shadow="true" data-iconshadow="true"
					data-wrapperels="span" class="ui-btn ui-shadow ui-btn-up-a"><span
					class="ui-btn-inner"><span class="ui-btn-text"> <img
							src="/images/icon-contact.png"><span>联系我们</span></span></span></a>
			</div>
			<div class="ui-block-b">
				<a href="aboutus.php#start" data-role="button" data-theme="a"
					data-corners="false" data-shadow="true" data-iconshadow="true"
					data-wrapperels="span" class="ui-btn ui-shadow ui-btn-up-a"><span
					class="ui-btn-inner"><span class="ui-btn-text"> <img
							src="/images/icon-about.png"><span>关于我们</span></span></span></a>
			</div>
		</div>
	</div>
	
	<?php include 'include/footer.php';?>

</body>
	
	<script type="text/javascript">
		$(document).ready(function() {
			commonUpdate();

			$("#homeNav").addClass("active");
		});
	</script>
</html>
