<!DOCTYPE html>
<html lang="en">
<head>
<?php include 'include/header.php'?>
<title>关于我们 - 云擎，移动网站专家</title>
</head>
<body>
	<?php include 'include/navigation_bar.php';?>

	<?php include 'include/carousel.php';?>

	<!-- Marketing messaging and featurettes
    ================================================== -->
	<!-- Wrap the rest of the page in another container to center all the content. -->
	<div class="container marketing">
		<!-- Three columns of text below the carousel -->
		<!-- /.row -->
		<div class="panel panel-primary">
			<div class="panel-heading">
				<a id="start" name="start"></a>
				<h3 class="panel-title">云擎从哪里来？</h3>
			</div>
			<div class="panel-body">
				云擎YunGine™是德厚科技TEQHOST™旗下移动事业部门，德厚创始人Mr.
				LV，计算机专业，标准的技术男，坚信科技可以改变世界。Mr.
				LV早年在广州电脑城捣鼓计算机硬件，之后转战系统集成，主攻服务器主机，德厚TEQHOST的名字即源于此。在PC互联网勃兴之时，Mr.
				LV顺势而为、软硬兼施，凭着过硬的设计与开发能力，在华南互联网应用行业中确立了自己的地位。</div>
		</div>
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h3 class="panel-title">云擎都有些神马样的人？</h3>
			</div>
			<div class="panel-body">作为代码牛人，Mr. LV从骨子里认同Google的Do not be
				evil企业信条，在德厚的发展过程中，不断吸引具有相同理念的产品经理、Web工程师、UI设计师、APP开发者、网页设计师加入到德厚平台。在服务成千优质客户的过程中，他们意识到并形成了德厚的使命：把程序技术的力量与人文艺术的设计理念完美结合，使客户的用户获得发自内心的良好体验。为顺应云计算与移动互联的趋势，德厚抽调技术骨干成立云擎移动事业部，虽然他们低调厚重，但他们是德厚技术牛人当中的牛人。
			</div>
		</div>
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h3 class="panel-title">云擎往哪里去？</h3>
			</div>
			<div class="panel-body">
				越来越多的人开始明白：看起来最简单的背后，其实一点都不简单。然而，只有极少数的公司能用技术+艺术的方式来实现，正如云擎事业部的工程师与设计师所做的那样。云擎认为：企业如个人一样必须有自己的个性，在移动互联时代，手机网站上的品牌尤其应该简单、清晰而有穿透力，无论在PC端还是移动终端上，再华丽的网站如果不利于搜索引擎抓取，也不过是虚有其表，而无实质意义。简单、极致用户体验、搜索引擎友好，更有利于流量转化，从而帮助企业获取更多市场机会，这就是云擎的努力方向。
			</div>
		</div>
		<div class="panel panel-primary">
			<div class="panel-heading">
				<a id="joinus" name="joinus"></a>
				<h3 class="panel-title">云擎的创意基因</h3>
			</div>
			<div class="panel-body">
				<p>云擎的高级产品经理Kristar与搜索引擎专家WinterSun，在工作之余喜爱运动、亲近自然，尤其热心公益慈善活动。Kristar于2001年发起成立LOH网络助学平台，是中国第一个点对点网络助学机构。云擎的工程师与设计师们几乎都是公益活动爱好者。他们认为生活就该丰富多彩有意思，这些理工科的技术控们，甚至打算组建己的钢琴吉他乐队。</p>
				<p>为此，Kristar发起成立了业余兴趣爱好小组创意基因iMagene，这里有原ORACLE美国总部的高级工程师，Microsoft中国的高级项目经理，TED的客座专家。iMagene第一阶段的主题是“设计让生活更精彩”。特别欢迎各类</p>
				
				<ul>
					<li>网页平面设计</li>
					<li>UI设计</li>
					<li>程序猿</li>
					<li>视频编辑制作牛人</li>
				</ul>
				加入iMagene，共享精彩人生。有意者请将个人代表作品发邮件至：img@yungine.com，邮件标题格式：姓名+专长+业余兴趣+加入iMagene。
			</div>
		</div>

		<div class="panel panel-primary">
			<div class="panel-heading">
				<a id="joinus" name="contactus"></a>
				<h3 class="panel-title">联系我们</h3>
			</div>
			<div class="panel-body">
				<!-- 
			<ul>
				<li>电话：<span class="label label-default">136-1145-8048</span></li>
				<li>地址：<span class="label label-default">广州市天河区五山路248号广州软件园金山大厦北塔1407室</span></li>
				<li>关注我们<img alt="二维码扫描" src="images/wechat.png"></li>
			</ul> -->
				<div class="row">
					<div class="col-xs-12 col-md-12">
						<div class="row">
							<div class="col-xs-12 text-left">
								<h5>
									联系人：殷先生
								</h5>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 text-left">
								<h5>
									电&nbsp;&nbsp;&nbsp;话：136-1145-8048
								</h5>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 text-left">
								<h5>
									邮&nbsp;&nbsp;&nbsp;箱：biz@yungine.com
								</h5>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 text-left">
								<h5>
									地&nbsp;&nbsp;&nbsp;址：广州市天河区 五山路248号广州软件园 金山大厦北塔1407室
								</h5>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-3 col-md-5 text-left">
								<h5>关注我们：</h5>
							</div>
							<div class="col-xs-9 col-md-7">
								<img alt="二维码扫描" src="images/wechat.png">
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>

	</div>
	<!-- /.container -->
	
	<?php include 'include/footer.php';?>
</body>
	
	<script type="text/javascript">
		$(document).ready(function() { 
			commonUpdate();

			$("#aboutNav").addClass("active");
		});
	</script>
</html>
