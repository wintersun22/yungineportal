var isMobileDevice = checkIsMobile();
function checkIsMobile() {
	var isMobile = {
		Android : function() {
			return navigator.userAgent.match(/Android/i) ? true : false;
		},
		BlackBerry : function() {
			return navigator.userAgent.match(/BlackBerry/i) ? true : false;
		},
		iOS : function() {
			return navigator.userAgent.match(/iPhone|iPad|iPod/i) ? true
					: false;
		},
		Windows : function() {
			return navigator.userAgent.match(/IEMobile/i) ? true : false;
		},
		any : function() {
			return (isMobile.Android() || isMobile.BlackBerry()
					|| isMobile.iOS() || isMobile.Windows());
		}
	};
	if (isMobile.any()) {
		return true;
	} else {
		return false;
	}
	
}

function commonUpdate()
{
	var suffix = "";
	if(isMobileDevice){
		$('#navButtonsDiv').show();
		$('#featuresDiv').hide();
		$('#navFooterDiv').hide();
		
		$('#copyrightMobileDiv').show();
		$('#copyrightPCDiv').hide();
		
		suffix = "-mobile";
		
		$("#navbarDiv").addClass("navbar-fixed-top");
	}
	else{
		$('#navButtonsDiv').hide();
		$('#featuresDiv').show();
		$('#navFooterDiv').show();
		
		$('#copyrightMobileDiv').hide();
		$('#copyrightPCDiv').show();

		$("#navbarDiv").addClass("navbar-static-top");
	}

	$("#banner1Image").attr('data-src', "");
	$("#banner2Image").attr('data-src', "");
	$("#banner3Image").attr('data-src', "");
	$("#banner1Image").attr('src', "images/banner1"+suffix+".jpg");
	$("#banner2Image").attr('src', "images/banner2"+suffix+".jpg");
	$("#banner3Image").attr('src', "images/banner3"+suffix+".jpg");	
	
	window.setTimeout(scrollPageForAnchor, 50);
	

}

function scrollPageForAnchor()
{
	var currTop = $(document).scrollTop();
	var hightOfNavBar = 70;
	//alert(currTop);
	if(currTop >= hightOfNavBar){
		$(document).scrollTop( currTop - hightOfNavBar );
	}
}
