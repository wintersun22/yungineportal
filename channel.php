<!DOCTYPE html>
<html lang="en">
<head>
<?php include 'include/header.php'?>

<title>渠道合作, 云擎, 广州建站, 移动建站, 云推广</title>
</head>
<body>
	<?php include 'include/navigation_bar.php';?>

	<?php include 'include/carousel.php';?>

	<!-- Marketing messaging and featurettes
    ================================================== -->
	<!-- Wrap the rest of the page in another container to center all the content. -->
	<div class="container marketing" id="channelDiv">

		<div class="panel panel-primary">
			<div class="panel-heading">
				<a id="start" name="start"></a>
				<h3 class="panel-title">为什么要成为渠道合作伙伴？</h3>
			</div>
			<div class="panel-body">
				<p>a. 产品：云擎的产品（解决方案）已被各行各业优质客户采用，在业内具有良好口碑，是您深挖行业财富的利器。</p>
				<p>b. 团队：云擎拥有资深互联网产品开发与设计人才，为合作商提供坚实的技术保障与放心服务。</p>
				<p>c. 公司：在企业互联网应用服务领域深耕十余载，愿与合作商为客户创造更大价值，从而实现合作商、客户、员工多赢的局面。</p>
				<p>d. 机会：有些事，你不为客户着想，总有你的竞争对手为他们着想，你能为客户提供更多附加的增值服务，客户就会为你带来更多的回报和价值。</p>
				 
			</div>
		</div>
		
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h3 class="panel-title">渠道合作伙伴利益</h3>
			</div>
			<div class="panel-body">
				<p>a. 把握云计算与移动互联趋势，抓住移动互联财富商机</p>
				<p>b. 依托云擎技术团队，为现有客户提供增值服务</p>
				<p>c. 深挖行业资源，共享合作利润分成</p>
				 
			</div>
		</div>
		
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h3 class="panel-title">合作模式</h3>
			</div>
			<div class="panel-body">
				<p>a. 项目合作方式：比较适合大客户的深度定制项目，数量相对较少，要求与客户有较深客情，但单个项目利润相对较高。</p>
				<p>b. 渠道代理方式：比较适合批量客户的常规需求，单个客户利润相对平均，但总利润较可观。</p>
				<p>具体合作事宜，请咨询QQ:70272702，136-1145-8048</p>
			</div>
		</div>
		
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h3 class="panel-title">谁适合成为渠道合作伙伴？</h3>
			</div>
			<div class="panel-body">
				<p>a. 品牌策划、营销策划、广告设计、文化传播公司</p>
				<p>b. 具有一定设计能力的商标代理、中介及交易平台</p>
				<p>c. 面向企业的教育培训及顾问咨询机构</p>
				<p>d. 工商财税代理、投资咨询公司</p>
			</div>
		</div>

	</div>
	<!-- /.container -->
	
	<?php include 'include/footer.php';?>
</body>
	
	<script type="text/javascript">
		$(document).ready(function() {
			$("#channelNav").addClass("active");
			
			commonUpdate();
		});
	</script>
</html>
